%option nounput
%option noinput

%{
#include "desk_calculator.tab.h"
extern int yylval;
%}

%%

[ \t]	;
[a-z]	{ yylval = yytext[0] - 'a'; return LETTER; }
[0-9]	{ yylval = yytext[0] - '0'; return DIGIT; }
.	|
\n	{ return yytext[0]; }

%%

