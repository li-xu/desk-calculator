PRG=desk_calculator
CFLAGS+=-std=gnu90
CFLAGS+=-Werror -Wextra -Wunused -Wall -pedantic-errors

# Following flag cause build failure,
# because bison generated C file
# contain conversions which may be unsafe.
# Only use following flag for user-generated code.
CFLAGS_USR+=-Wconversion

all: $(PRG) precommit_check deheader deheader_pass cppcheck cppcheck_pass

$(PRG): $(PRG).tab.o $(PRG).yy.o $(PRG).o
	gcc $(CFLAGS) $(CFLAGS_USR) -o $@ $^

# YACC
$(PRG).tab.c $(PRG).tab.h: $(PRG).y
	bison -d $<

$(PRG).tab.o: $(PRG).tab.c $(PRG).tab.h
	gcc $(CFLAGS) -c $< -o $@

# LEX
$(PRG).yy.c: $(PRG).l
	flex --outfile=$@ $<

$(PRG).yy.o: $(PRG).yy.c $(PRG).tab.h
	gcc $(CFLAGS) -c $< -o $@

# USER
$(PRG).o: $(PRG).c $(PRG).tab.h
	gcc $(CFLAGS) $(CFLAG_USR) -c $< -o $@

precommit_check: precommit_check.c
	gcc $(CFLAGS) $(CFLAGS_USR) -o $@ $<

deheader: $(PRG).c precommit_check.c
	deheader $^;

deheader_pass:
	echo "" > deheader

cppcheck: $(PRG).c precommit_check.c
	cppcheck --error-exitcode=1 --enable=warning,performance $^;

cppcheck_pass:
	echo "" > cppcheck

clean:
	rm -f $(PRG).tab.c
	rm -f $(PRG).tab.h
	rm -f $(PRG).tab.o
	rm -f $(PRG).yy.c
	rm -f $(PRG).yy.o
	rm -f $(PRG).o
	rm -f $(PRG)
	rm -f precommit_check
	rm -f deheader
	rm -f cppcheck
