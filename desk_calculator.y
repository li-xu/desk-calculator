/*
* Desk calculator example from YACC paper by S.C. Johnson
* http://dinosaur.compilertools.net/yacc/index.html
*
* Calculator can perform arithmetic '+', '-', '*', '/'
* Result can be saved to variables 'a' to 'z' using '='
*/
%{
#include <stdio.h>

extern int yylex();
extern void yyerror (char const *);

int regs[26];	/* There are 26 English letters */
int base;
%}

%start list

%token DIGIT LETTER

%left '|'
%left '&'
%left '+' '-'
%left '*' '/' '%'
%left UMINUS	/* Supplies precedence for unary minus */

%%	/* Rules section beginning */

list	:	/* Empty */
	|	list	stat	'\n'
	|	list	error	'\n'
			{	yyerrok;	}
	;

stat	:	expr
			{	printf("%d\n", $1);	}
	|	LETTER	'='	expr
			{	regs[$1] = $3;	}
	;

expr	:	'('	expr	')'
			{	$$ = $2;	}

	|	expr	'+'	expr
			{	$$ = $1 + $3;	}
	|	expr	'-'	expr
			{	$$ = $1 - $3;	}
	|	expr	'*'	expr
			{	$$ = $1 * $3;	}
	|	expr	'/'	expr
			{	$$ = $1 / $3;	}
	|	expr	'%'	expr
			{	$$ = $1 % $3;	}
	|	expr	'&'	expr
			{	$$ = $1 & $3;	}
	|	expr	'|'	expr
			{	$$ = $1 | $3;	}
	|	'-'	expr	%prec UMINUS
			{	$$ = - $2;	}
	|	LETTER
			{	$$ = regs[$1];	}
	|	number
	;

number	:	DIGIT
			{	$$ = $1; base = ($1 == 0) ? 8 : 10;	}
	|	number	DIGIT
			{	$$ = base * $1 + $2;	}

%%	/* Program beginning */
