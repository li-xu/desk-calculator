Desk calculator example from YACC paper by S.C. Johnson
http://dinosaur.compilertools.net/yacc/index.html

To run the program::

        $ make
        $ ./desk_calculator

The program is an integer arithmetic calculator.
