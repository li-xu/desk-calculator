#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define DEBUG		1

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define CMDSIZE		512
#define BUFSIZE		2056

/* If there are whitespace errors,
 * print the offending file names and fail.
 * Argument is commit reference.
 */
#define WHITESPACE_CHK	"git diff-index --check --cached %s"

static int exec(char* cmd)
{
	FILE *fd;
	char buf[BUFSIZE];
	int ret;

	if (DEBUG)
		fprintf(stdout, "cmd = %s\n", cmd);

	fd = popen(cmd, "r");
	if (!fd) {
		fprintf(stderr, "popen error (%d)\n", errno);
		return errno;
	}

	while (fgets(buf, BUFSIZE, fd)) {
		fprintf(stdout, "%s", buf);
	}

	ret = pclose(fd);
	if (ret == -1) {
		fprintf(stderr, "pclose error (%d)\n", errno);
		return errno;
	}

	if (DEBUG)
		fprintf(stdout, "ret = %d\n", ret);

	return ret;
}

static char *cmds[] = {
	WHITESPACE_CHK,
	"git stash save --keep-index",
	"make clean",
	"make all",
	"make clean",
	"git stash pop",
};

/*
 * Input argument is Git commit to verify before committing
 */
int main(int argc, char *argv[])
{
	char cmd[CMDSIZE];
	int ret;
	unsigned int i;

	if (argc != 2) {
		fprintf(stderr, "Wrong number of input arguments (%d)\n", argc);
		return EXIT_FAILURE;
	}

	for (i = 0; i < ARRAY_SIZE(cmds); i++) {
		if (i == 0) {
			/* White space check requires commit reference */
			snprintf(cmd, CMDSIZE, cmds[i], argv[1]);
			ret = exec(cmd);
		} else {
			ret = exec(cmds[i]);
		}

		if (ret)
			return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
