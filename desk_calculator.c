#include <stdio.h>

#include "desk_calculator.tab.h"

int yywrap()
{
	return 1;
}

void yyerror(char const * s)
{
	fprintf(stderr, "%s\n", s);
}

int main()
{
	return yyparse();
}
